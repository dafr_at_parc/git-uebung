Exercise:

Explain the following commands with one sentence:

    $ git checkout work             Mit diesem Befehl wechselt man auf den Branch mit Namen 'work'.
    $ git checkout master           Mit diesem Befehl wechselt man von einem Branch auf den master-Branch.
    $ git merge work                Mit diesem Befehl wird der Merge Prozess vom Branch 'work' in den aktuell ausgecheckten Branch gestartet.
    $ git merge --abort             Bei Auftreten eines Merge-Konfliktes kann hiermit der merge-Prozess abgebrochen werden.
    $ git cat-file -p a3798b        Befindet man sich im .git/objects/{xy} Ordner (wobei {xy} für die ersten beiden Zeichen eines Fingerprints
                                    (git-Hash-Code) steht), wird mit diesem Befehl der Inhalt des Git-Objectes mit diesem Fingerprint angezeigt. Dies kann der Inhalt einer Datei oder beispielsweise der Inhalt eines Commits sein.

Example:

    $ git status         Shows the current state of the repository


Here are some standard situations, and a git command. Draw the result, and add some explanation:

Example 1:

    A - B (master)
         \
          C (work)

    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird der 'work' Branch in den master-branch gemerged. Entspricht einem git push bzw. fast-forward, da sich der master-branch noch im gleichen Zustand befindet, wie zum Zeitpunkt der Branch-Erstellung.
  Ergebnis:

    A - B - C(master/work)

Example 2:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird wieder der 'work' Branch in den master-branch gemerged. Diesmal wurden aber in beiden Branches commits eingefügt. Im Rahmen des merges wird ein merge-Commit angelegt. ggf. müssen Konflikte gelöst werden.
  Ergebnis:

    A - B - C - D - E (master)
         \         /
           X - Y (work)


Example 3:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird wieder der 'work' Branch in den master-branch gemerged. Dabei wird aber im ersten Teil die Arbeit auf dem master-branch dem work-Branch hizugefügt. Dadurch wird die Situation aus example1 hergestellt.ggf. auftretende Konflikte werden beim merge des masters in den work-Branch gelöst
  Ergebnis:

    Schritt 1
    A - B - C - D (master)
          \       \
            X - Y - Z (work)

    Schritt 2
    A - B - C - D
          \       \
            X - Y - Z (master / work)


First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.